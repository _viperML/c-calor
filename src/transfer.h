#ifndef HEAT_H
#define HEAT_H

#include <stdint.h>

#include "mesh.h"

// Calculate new temp at position
float transfer_heat(mesh* _mesh, uint32_t x, uint32_t y);

typedef enum {
    FORWARD_DERIVARIVE = 0,
    CENTRAL_DERIVATIVE = 1,
    BACKWARD_DERIVATIVE = 2,
} derivative_mode;

float second_derivative(float* elems, size_t index, derivative_mode mode);

#endif
