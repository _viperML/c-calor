#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "mesh.c"
#include "transfer.h"

float apply_initial_conditions(mesh* _mesh, uint32_t x, uint32_t y) {
  if (x == 0) {
    return 2.0;
  } else if (y == 0) {
    return 4.0;
  } else {
    return _mesh->u[x][y];
  }
}

float fixed_condition(mesh* _mesh, uint32_t x, uint32_t y) {
  if (x == 0) {
    return 2.0;
  } else if (y == 0) {
    return 4.0;
  } else {
    return _mesh->u[x][y];
  }
}

int main() {
  mesh my_mesh = {0};
  uint32_t my_mesh_size = 100;

  mesh_init(&my_mesh, my_mesh_size, my_mesh_size);

  printf("---- initial conditions\n");
  mesh_apply(&my_mesh, apply_initial_conditions);
  mesh_preview(&my_mesh, 10);

  for (uint32_t iter = 0; iter < 500; iter++) {
    mesh_apply(&my_mesh, transfer_heat);
    mesh_apply(&my_mesh, fixed_condition);
    // printf("\n\n");
    // printf("---- iter %d\n", iter);
    // mesh_preview(&my_mesh, my_mesh_size);
  }

  mesh_preview(&my_mesh, 10);

  mesh_destroy(&my_mesh);
  return EXIT_SUCCESS;
}
