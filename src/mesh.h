#ifndef MESH_H
#define MESH_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#define MESH_INIT_U 0.0
#define MESH_DELTA_T 1.0

typedef struct mesh {
  uint32_t size_x;
  uint32_t size_y;

  float alpha;

  float** u;
  float** u_next;
} mesh;

typedef float (*mesh_applicable)(mesh*, uint32_t x, uint32_t y);

int mesh_init(mesh* _mesh, uint32_t size_x, uint32_t size_y);
int mesh_destroy(mesh* _mesh);

float** init_mesh_u(uint32_t size_x, size_t size_y);

// Sets u to u_next
void mesh_commit(mesh* _mesh);

void mesh_preview(mesh* _mesh, uint32_t items);

void mesh_apply(mesh* _mesh, mesh_applicable);

#endif
