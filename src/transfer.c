#include <stdint.h>
#include <stdio.h>

#include "mesh.h"
#include "transfer.h"

float transfer_heat(mesh* _mesh, uint32_t x, uint32_t y) {

  float result = 0;
  float* elems = malloc(3 * sizeof(float));
  size_t index;
  derivative_mode mode;

  // Compute x derivative
  float delta_x;
  if (x == 0) {
    mode = FORWARD_DERIVARIVE;
    index = 0;
    elems[0] = _mesh->u[x][y];
    elems[1] = _mesh->u[x + 1][y];
    elems[2] = _mesh->u[x + 2][y];
  } else if (x == _mesh->size_x - 1) {
    mode = BACKWARD_DERIVATIVE;
    index = 2;
    elems[0] = _mesh->u[x - 2][y];
    elems[1] = _mesh->u[x - 1][y];
    elems[2] = _mesh->u[x][y];
  } else {
    mode = CENTRAL_DERIVATIVE;
    index = 1;
    elems[0] = _mesh->u[x - 1][y];
    elems[1] = _mesh->u[x][y];
    elems[2] = _mesh->u[x + 1][y];
  }
  delta_x = second_derivative(elems, index, mode);

  // Compute y derivative
  float delta_y;
  if (y == 0) {
    mode = FORWARD_DERIVARIVE;
    index = 0;
    elems[0] = _mesh->u[x][y];
    elems[1] = _mesh->u[x][y+1];
    elems[2] = _mesh->u[x][y+2];
  } else if (y == _mesh->size_y - 1) {
    mode = BACKWARD_DERIVATIVE;
    index = 2;
    elems[0] = _mesh->u[x][y-2];
    elems[1] = _mesh->u[x][y-1];
    elems[2] = _mesh->u[x][y];
  } else {
    mode = CENTRAL_DERIVATIVE;
    index = 1;
    elems[0] = _mesh->u[x][y-1];
    elems[1] = _mesh->u[x][y];
    elems[2] = _mesh->u[x][y+1];
  }
  delta_y = second_derivative(elems, index, mode);

  result = _mesh->u[x][y] + _mesh->alpha * (delta_x + delta_y);

  free(elems);
  return result;
}

float second_derivative(float* elems, size_t index, derivative_mode mode) {
  float result;

  if (mode == CENTRAL_DERIVATIVE) {
    result = elems[index + 1] - 2 * elems[index] + elems[index - 1];
  } else if (mode == FORWARD_DERIVARIVE) {
    result = elems[index + 2] - 2 * elems[index + 1] + elems[index];
  } else {
    result = elems[index] - 2 * elems[index - 1] + elems[index - 2];
  }

  return result;
}
