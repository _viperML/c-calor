#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "mesh.h"

float** init_mesh_u(uint32_t size_x, size_t size_y) {
  float** u = malloc(size_x * sizeof(u));

  for (size_t x = 0; x < size_x; x++) {
    u[x] = malloc(size_y * sizeof(u));

    for (size_t y = 0; y < size_y; y++) {
      u[x][y] = MESH_INIT_U;
    }
  }

  return u;
}

int mesh_init(mesh* _mesh, uint32_t size_x, uint32_t size_y) {
  _mesh->alpha = 0.1f;

  _mesh->size_x = size_x;
  _mesh->size_y = size_y;

  _mesh->u = init_mesh_u(_mesh->size_x, _mesh->size_y);
  _mesh->u_next = init_mesh_u(_mesh->size_x, _mesh->size_y);

  return 0;
}

int mesh_destroy(mesh* _mesh) {
  for (size_t x = 0; x < _mesh->size_x; x++) {
    free(_mesh->u[x]);
    free(_mesh->u_next[x]);
  }
  free(_mesh->u);
  free(_mesh->u_next);

  return 0;
}

void mesh_preview(mesh* _mesh, uint32_t items) {
  printf("\n");

  printf("Mesh size: %dx%d\n", _mesh->size_x, _mesh->size_y);
  printf("alpha: %f\n", _mesh->alpha);

  printf("\n");

  // Print from top (y) to bottom
  for (uint32_t y = items - 1; y >= 0 && y < items; y--) {
    printf("%d  ", y);
    for (uint32_t x = 0; x < items; x++) {
      printf("%.2f  ", _mesh->u[x][y]);
    }
    printf("\n");
  }
  printf(" ");
  for (uint32_t x = 0; x < items; x++) {
    printf("  %-4d", x);
  }
  printf("\n");
}

void mesh_commit(mesh* _mesh) {
  for (uint32_t x = 0; x != _mesh->size_x; x++) {
    for (uint32_t y = 0; y != _mesh->size_y; y++) {
      _mesh->u[x][y] = _mesh->u_next[x][y];
    }
  }
}

void mesh_apply(mesh* _mesh, mesh_applicable func) {
  for (uint32_t x = 0; x != _mesh->size_x; x++) {
    for (uint32_t y = 0; y != _mesh->size_y; y++) {
      _mesh->u_next[x][y] = func(_mesh, x, y);
    }
  }

  mesh_commit(_mesh);
}
