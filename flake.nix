{
  description = "flake-parts based template";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.05";
    flake-parts = {
      url = "github:hercules-ci/flake-parts";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs @ {
    self,
    nixpkgs,
    flake-parts,
    ...
  }:
    flake-parts.lib.mkFlake {inherit self;} {
      systems = nixpkgs.lib.systems.flakeExposed;
      flake.overlays.default = final: prev: {
        c-calor = final.callPackage ./package.nix {};
      };
      perSystem = {
        pkgs,
        system,
        config,
        ...
      }: {
        _module.args.pkgs = import nixpkgs {
          inherit system;
          overlays = [self.overlays.default];
        };
        packages = {
          inherit (pkgs) c-calor;
          default = config.packages.c-calor;

          c-calor-develop =
            (config.packages.c-calor.overrideAttrs (old: {
              nativeBuildInputs =
                old.nativeBuildInputs
                ++ (with pkgs; [
                  clang-tools
                  asciidoctor
                  lldb
                ]);
            }))
            .override {
              # stdenv = pkgs.clangStdenv;
            };
        };
      };
    };
}
