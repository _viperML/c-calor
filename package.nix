{
  stdenv,
  lib,
  cmake,
  ninja,
}:
stdenv.mkDerivation {
  name = "c-calor";

  src = lib.cleanSource ./.;

  nativeBuildInputs = [
    cmake
  ];
}
